package ali.example.com.ydsozluk.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ali.example.com.ydsozluk.R;
import ali.example.com.ydsozluk.models.TokenModel;

/**
 * Created by ali on 17.01.2018.
 */

public class AdapterSyntaxRecycler extends RecyclerView.Adapter<AdapterSyntaxRecycler.MyViewHolder> {
    private List<TokenModel> tokenModels = new ArrayList<>();
    private Context mContext;

    public AdapterSyntaxRecycler() {
        super();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_syntax_row,parent,false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        TokenModel tokenModel = tokenModels.get(position);
        holder.label.setText(tokenModel.getDependencyEdge().getLabel());
        holder.text.setText(tokenModel.getText().getContent());
        holder.tag.setText(tokenModel.getPartOfSpeech().getTag());

    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        TextView text,tag,label;

        MyViewHolder(View itemView) {
            super(itemView);
            text = itemView.findViewById(R.id.dependency_text);
            tag = itemView.findViewById(R.id.part_of_speech_tag);
            label = itemView.findViewById(R.id.dependency_label);
        }
    }



    @Override
    public int getItemCount() {
        return tokenModels.size();
    }

    public AdapterSyntaxRecycler(List<TokenModel> tokenModels, Context mContext) {
        this.tokenModels = tokenModels;
        this.mContext = mContext;
    }


}

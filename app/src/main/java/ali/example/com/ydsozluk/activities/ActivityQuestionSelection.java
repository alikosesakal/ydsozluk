package ali.example.com.ydsozluk.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import ali.example.com.ydsozluk.R;
import ali.example.com.ydsozluk.adapters.AdapterQuestionsRecycler;
import ali.example.com.ydsozluk.models.Question;
import ali.example.com.ydsozluk.utils.RecyclerItemClickListener;

public class ActivityQuestionSelection extends AppCompatActivity implements ChildEventListener,
        RecyclerItemClickListener.OnItemClickListener {
    private String TAG = ActivityQuestionSelection.class.getSimpleName();
    private DatabaseReference mDatabaseReference;
    private FirebaseDatabase mFirebaseDatabase;
    private String year, semester, activityTitle;
    private List<Question> questionList = new ArrayList<>();
    private RecyclerView questionsRecyclerView;
    private AdapterQuestionsRecycler recyclerAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_selection);

        if (savedInstanceState == null){
            Intent i = getIntent();
            semester = i.getStringExtra("semester");
            year = i.getStringExtra("year");
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            if (semester.equals("spring")){
                activityTitle = "İlkbahar";
            }else {
                activityTitle = "Güz";
            }
            getSupportActionBar().setTitle(year + " " + activityTitle);
        }

        initRecyclerView();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mDatabaseReference = mFirebaseDatabase.getReference("TranslateQuestions").child(year).child(semester);
        mDatabaseReference.addChildEventListener(this);

    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("semester",semester);
        outState.putString("year",year);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        semester = savedInstanceState.getString("semester");
        year = savedInstanceState.getString("year");
    }

    public void initRecyclerView() {
        questionsRecyclerView = findViewById(R.id.recycler_questions);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        questionsRecyclerView.setHasFixedSize(true);
        questionsRecyclerView.setLayoutManager(mLayoutManager);
        questionsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerAdapter = new AdapterQuestionsRecycler(questionList,getApplicationContext());
        questionsRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getApplicationContext(),this));
        questionsRecyclerView.setAdapter(recyclerAdapter);
    }

    @Override
    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        questionList.add(dataSnapshot.getValue(Question.class));
        recyclerAdapter.notifyDataSetChanged();
    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
        Question q = dataSnapshot.getValue(Question.class);
        int index = getItemIndex(q);
        questionList.set(index, q);
        recyclerAdapter.notifyItemChanged(index);
    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {

    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onCancelled(DatabaseError databaseError) {

    }

    public int getItemIndex(Question question) {
        int index = -1;
        for (int i = 0; i < questionList.size(); i++) {
            if (questionList.get(i).getQuestionNumber() == question.getQuestionNumber()) {
                index = i;
                break;
            }
        }
        return index;
    }

    @Override
    public void onItemClick(View view, int position) {
        Intent i = new Intent(ActivityQuestionSelection.this,ActivityQuestion.class);
        Question q = questionList.get(position);
        i.putExtra("pharagraphING",q.getPharagraphING());
        i.putExtra("pharagraphTR",q.getPharagraphTR());
        i.putExtra("questionNumber",q.getQuestionNumber());
        startActivity(i);
    }
}

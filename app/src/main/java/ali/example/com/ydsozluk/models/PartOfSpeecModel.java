package ali.example.com.ydsozluk.models;


public class PartOfSpeecModel {
    private String tag;
    private String number;
    private String voice;

    public PartOfSpeecModel() {
        super();
    }

    public PartOfSpeecModel(String tag, String number, String voice) {
        super();
        this.tag = tag;
        this.number = number;
        this.voice = voice;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getVoice() {
        return voice;
    }

    public void setVoice(String voice) {
        this.voice = voice;
    }
}

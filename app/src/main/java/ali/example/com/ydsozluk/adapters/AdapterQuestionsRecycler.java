package ali.example.com.ydsozluk.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import java.util.List;

import ali.example.com.ydsozluk.R;
import ali.example.com.ydsozluk.models.Question;

/**
 * Created by ali on 12.12.2017.
 */

public class AdapterQuestionsRecycler extends RecyclerView.Adapter<AdapterQuestionsRecycler.MyViewHolder> {

    private List<Question> questionList;
    private Context mContext;

    public AdapterQuestionsRecycler(List<Question> questionList, Context context) {
        this.questionList = questionList;
        mContext = context;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_question_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Question q = questionList.get(position);
        String s = mContext.getResources().getString(R.string.text_question_heading, q.getQuestionNumber());
        holder.questionTitle.setText(s);
        holder.questionBody.setText(q.getPharagraphING());
    }

    @Override
    public int getItemCount() {
        return questionList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView questionBody, questionTitle;
        private final View itemView;

        MyViewHolder(View itemView) {
            super(itemView);
            questionTitle = itemView.findViewById(R.id.text_question_title);
            questionBody = itemView.findViewById(R.id.text_question_body);
            this.itemView = itemView;
        }

    }


    public List<Question> getQuestionList() {
        return questionList;
    }

    public void setQuestionList(List<Question> questionList) {
        this.questionList = questionList;
    }
}

package ali.example.com.ydsozluk.activities;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Spinner;

import ali.example.com.ydsozluk.R;


public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private String TAG = MainActivity.class.getSimpleName();
    private Spinner yearsSpinner;
    private RadioGroup radioGroup;
    private String selectedYear, selectedSemester;
    private Button buttonGetQuestions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }

    @Override
    protected void onStart() {
        super.onStart();

        yearsSpinner = findViewById(R.id.years_spinner);
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.years_spinner_items, android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        yearsSpinner.setAdapter(spinnerAdapter);
        yearsSpinner.setOnItemSelectedListener(this);

        buttonGetQuestions = findViewById(R.id.button_get_questions);
        buttonGetQuestions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, ActivityQuestionSelection.class);
                i.putExtra("year", selectedYear);
                i.putExtra("semester", selectedSemester);
                startActivity(i);
            }
        });

        radioGroup = findViewById(R.id.semester_radio_group);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.spring) {
                    selectedSemester = "spring";
                    Log.e(TAG + " ==>> ", "Selected semester is Spring");
                } else if (i == R.id.autumn) {
                    selectedSemester = "autumn";
                    Log.e(TAG + " ==>> ", "Selected semester is Autumn");
                }
            }
        });

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        String item = adapterView.getSelectedItem().toString();
        selectedYear = item;
        Log.e(TAG + " ==>> ", item);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}

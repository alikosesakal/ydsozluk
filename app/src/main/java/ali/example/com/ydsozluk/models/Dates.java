package ali.example.com.ydsozluk.models;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by ali on 10.12.2017.
 */
@IgnoreExtraProperties
public class Dates {
    private ArrayList<String> semesters;
    private ArrayList<String> years;

    public Dates(){

    }

    public ArrayList<String> getSemesters() {
        return semesters;
    }

    public void setSemesters(ArrayList<String> semesters) {
        this.semesters = semesters;
    }

    public ArrayList<String> getYears() {
        return years;
    }

    public void setYears(ArrayList<String> years) {
        this.years = years;
    }
}

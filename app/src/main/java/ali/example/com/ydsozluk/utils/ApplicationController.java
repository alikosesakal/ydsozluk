package ali.example.com.ydsozluk.utils;

import android.app.Application;

/**
 * Created by ali on 27.12.2017.
 */

public class ApplicationController extends Application {
    private static final int SEMESTER_AUTUMN_ID = 0;
    private static final int SEMESTER_SPRING_ID = 1;
    private static final String API_KEY = "AIzaSyCVSDEfdjmem2U_NYBvfN2YX6exPhTwld4";

    public static final String TAG = ApplicationController.class.getSimpleName();
    public static final String ANALYZE_URL = "http://192.168.1.12:8080/webapi/myresource/syntax";
    public static final String SIMPLE_REQUEST = "http://192.168.1.12:8080/webapi/myresource/simple";
    public static final String ANALYZE_URL_MOBILE = "http://192.168.43.175:8080/webapi/myresource/syntax";

    private static ApplicationController mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

    public static synchronized ApplicationController getInstance() {
        return mInstance;
    }


    public static int getSemesterAutumnId() {
        return SEMESTER_AUTUMN_ID;
    }

    public static int getSemesterSpringId() {
        return SEMESTER_SPRING_ID;
    }

    public static String getApiKey() {
        return API_KEY;
    }
}

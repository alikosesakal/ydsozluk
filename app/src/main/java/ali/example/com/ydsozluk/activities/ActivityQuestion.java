package ali.example.com.ydsozluk.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.util.ArrayList;
import java.util.List;

import ali.example.com.ydsozluk.R;
import ali.example.com.ydsozluk.adapters.AdapterSyntaxRecycler;
import ali.example.com.ydsozluk.models.Question;
import ali.example.com.ydsozluk.models.RootResponseObject;
import ali.example.com.ydsozluk.models.TokenModel;
import ali.example.com.ydsozluk.utils.ApplicationController;
import cz.msebera.android.httpclient.Header;

public class ActivityQuestion extends AppCompatActivity {
    private Question question;
    private TextView pharagraph;
    private Button button;
    private ProgressDialog progressDialog;
    private RecyclerView syntaxRecycler;
    private AdapterSyntaxRecycler adapterSyntaxRecycler;
    private List<TokenModel> tokenModels = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Lütfen Bekleyin...");
        progressDialog.setCancelable(false);

        button = findViewById(R.id.analyze_button);

        syntaxRecycler = findViewById(R.id.syntax_recycler);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        syntaxRecycler.setHasFixedSize(true);
        syntaxRecycler.setLayoutManager(mLayoutManager);
        syntaxRecycler.setItemAnimator(new DefaultItemAnimator());
        adapterSyntaxRecycler = new AdapterSyntaxRecycler(tokenModels,getApplicationContext());
        syntaxRecycler.setAdapter(adapterSyntaxRecycler);

        Intent intent = getIntent();
        question = new Question(intent.getStringExtra("pharagraphING"),
                intent.getStringExtra("pharagraphTR"),
                intent.getLongExtra("questionNumber", 0));

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(getApplicationContext().getResources().
                    getString(R.string.text_question_heading, question.getQuestionNumber()));
        }

        pharagraph = findViewById(R.id.text_question);
        pharagraph.setText(question.getPharagraphING());

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeAnOtherRequest(question.getPharagraphING());
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void makeAnOtherRequest(final String pharagraph){
        showpDialog();

        RequestParams params = new RequestParams();
        params.put("pharagraph",pharagraph);
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(ApplicationController.ANALYZE_URL_MOBILE,params,new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                RootResponseObject root = new Gson().fromJson(response.toString(),
                        new TypeToken<RootResponseObject>(){}.getType());
                tokenModels.addAll(root.getTokenModels());
                adapterSyntaxRecycler.notifyDataSetChanged();
                Log.e("JSONOBJECT == >> ",response.toString());
                hidepDialog();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                Log.e("Error Response ==>> ",responseString);
                hidepDialog();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Log.e("Error Response ==>> ",errorResponse.toString());
                hidepDialog();
            }
        });

    }

    private void showpDialog() {
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    private void hidepDialog() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }
}

package ali.example.com.ydsozluk.models;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by ali on 10.12.2017.
 */
@IgnoreExtraProperties
public class Question {
    private String pharagraphTR;
    private String pharagraphING;
    private long questionNumber;

    public Question() {

    }

    public Question(String pharagraphING, String pharagraphTR, long questionNumber){
        this.pharagraphING = pharagraphING;
        this.pharagraphTR = pharagraphTR;
        this.questionNumber = questionNumber;
    }

    public String getPharagraphTR() {
        return pharagraphTR;
    }

    public void setPharagraphTR(String pharagraphTR) {
        this.pharagraphTR = pharagraphTR;
    }

    public String getPharagraphING() {
        return pharagraphING;
    }

    public void setPharagraphING(String pharagraphING) {
        this.pharagraphING = pharagraphING;
    }

    public long getQuestionNumber() {
        return questionNumber;
    }

    public void setQuestionNumber(long questionNumber) {
        this.questionNumber = questionNumber;
    }
}
